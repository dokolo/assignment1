trigger TriggerOnContractClause on Contract_Clause__c (after insert, before delete) {
    
    //Trigger to insert recrods into Junction Object
    if( Trigger.isInsert && Trigger.isAfter)
    {
        List<Contracts_Clauses__c> CCList = new List<Contracts_Clauses__c>();
        for(Contract_Clause__c cc : Trigger.New)
        {
            Contracts_Clauses__c ContractClause = new Contracts_Clauses__c();
            ContractClause.Contract_Clause__c = cc.id;
            ContractClause.Contract__c = cc.Contract__c;
            CCList.add(ContractClause);
        }
        
        if(CCList.size() > 0)
        {
            insert CCList;
        }
    }
}