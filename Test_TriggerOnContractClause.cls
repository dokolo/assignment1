@isTest
public class Test_TriggerOnContractClause {
    
    @isTest
    public static void createContractClause()
    {
        Account a = new Account( name = 'Test');
        insert a;
        
        Contract c = new Contract(AccountId=a.id);
        insert c;
        
        Contract_Clause__c cc = new Contract_Clause__c(name='TestClause', Contract__c = c.id);
        insert cc;
            
    }

}